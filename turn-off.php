<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/settings.php';
use \Curl\Curl;

error_reporting(E_ALL);
ini_set('display_errors', '1');

$curl = new Curl();
$curl->post("https://api.particle.io/v1/devices/$particle_device_id/color", array(
    'arg' => 'off',
    'access_token' =>  $particle_access_token
));
