<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/settings.php';
use \Curl\Curl;

error_reporting(E_ALL);
ini_set('display_errors', '1');

$status = "green";

// Loop through all the hosts
foreach($hosts as $host) {
    $curl = new Curl();
    $curl->get("http://$host:19999/api/v1/alarms");

    if (!$curl->error && isset($curl->response->alarms)) {
        foreach($curl->response->alarms as $alarm) {
            if($alarm->status === 'WARNING' && $status === "green") {
                $status = "orange";
            } else if ($alarm->status === 'CRITICAL') {
                $status = "red";
                break;
            } 
        }
        
        // If the status is critical, break
        if($status === "red") {
            break;
        }
    }

    // Turn red if the curl get is unsucessful
    else {
	$status = "red";
	break;
    }
}

$curl = new Curl();
$curl->post("https://api.particle.io/v1/devices/$particle_device_id/color", array(
    'arg' => $status,
    'access_token' =>  $particle_access_token
));
